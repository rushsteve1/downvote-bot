(ns downvote-bot.turnips
  (:gen-class)
  (:require
   [clojure.data.csv :as csv]
   [clojure.java.io :as io])
  (:import [java.time
            DayOfWeek
            Instant
            LocalDate
            ZoneId
            ZoneOffset
            ZonedDateTime
            format.TextStyle]
           [java.util Locale]))

(defonce turnip-file "turnips.csv")

(defn add-price! [user price]
  (with-open [writer (io/writer turnip-file :append true)]
    (csv/write-csv writer [[(System/currentTimeMillis) user price]])))

(defn load-prices []
  (with-open [reader (io/reader turnip-file)]
    (reduce (fn [acc [time user price]]
              (assoc acc (read-string time)
                     [user (read-string price)]))
            (sorted-map)
            (csv/read-csv reader))))

(defn beginning-of-week []
  (let [today (LocalDate/now)
        day-num (.getValue (.getDayOfWeek today))
        days-since-sun (- 7 (- 7 day-num))]
    (-> (LocalDate/now)
        (.minusDays (if (> 7 days-since-sun) days-since-sun 0))
        (.atStartOfDay)
        (.atZone (ZoneId/systemDefault))
        (.toEpochSecond)
        (* 1000))))

;; https://www.dotkam.com/2015/12/02/time-series-database-in-one-line-of-clojure/

(defn this-week [prices]
  (into (sorted-map) (subseq prices > (beginning-of-week))))

(defn user-prices [user prices]
  (into (sorted-map) (filter #(= (first (second %)) user) prices)))

(defn nice-time [time]
  (let [instant (Instant/ofEpochMilli time)
        time-obj (ZonedDateTime/ofInstant instant (ZoneId/systemDefault))]
    (str (.getDisplayName
          (.getDayOfWeek time-obj)
          (TextStyle/FULL)
          (Locale/getDefault))
         " "
         (if (< (.getHour time-obj) 12) "AM" "PM"))))
