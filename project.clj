(defproject downvote-bot "0.3.1"
  :description "A bot that removes downvoted posts on Discord, among other things"
  :url "https://git.rushsteve1.us/rushsteve1/downvote-bot/"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "1.1.587"]
                 [org.clojure/data.csv "1.0.0"]
                 [org.suskalo/discljord "0.2.8"]]
  :main ^:skip-aot downvote-bot.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
