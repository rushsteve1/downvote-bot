(ns downvote-bot.bigmoji
  (:gen-class)
  (:require
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]))

(defn parse-reg [content]
  (first (rest (re-matches #"^:?(\w+):?$" content))))

(defn parse-mesg [content]
  (rest (re-matches #".*:(\w+):.*" content)))

(defn recall []
  (with-open [reader (io/reader "bigmoji.csv")]
    (for [row (doall (csv/read-csv reader))]
      (first row))))

(defn retrieve [name]
  (with-open [reader (io/reader "bigmoji.csv")]
    (second
     (first
      (filter
       (fn [e] (= (first e) name))
       (csv/read-csv reader))))))

(defn register! [name text]
  (with-open [writer (io/writer "bigmoji.csv" :append true)]
    (csv/write-csv writer [[name text]])))
